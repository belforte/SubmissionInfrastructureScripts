#!/usr/bin/python
from __future__ import print_function, division

import htcondor
import classad

import sys
import os
import pwd
import json
import httplib, urllib2
import pprint
import logging
import warnings
import JobAutoTunerAlarms
import array

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot

import numpy as np

from JobAutoTunerAlarms import *

import pdb

import time
from datetime import datetime
from socket import gethostname
from  collections import deque


import requests
import ast


# Setting log file location and formatting details
LOG_FILE = '/var/log/crab/JobAutoTunerView.log'

# todo - separate it in different classes - the JobAutoTunerView class should
#        provide just the methods for visualisation && to have different classes
#        for any different type of script called by the JobAutoTuner.py which
#        should be passed as a parameter to the JobAutoTunerView constructor

# todo - the following class ot be used when(if) we shift to sending not a single
#        document per all the jobs in the schedd, but to a documment per job
#        and making the aggregation and histogram ploting in kibana(grafana)

class JsonDoc:
    def __init__(self,type):
        self.type = type
        self.valDict = dict()
        self.document = dict(
            producer="crab",
            type=self.type,
            hostname=gethostname()
        )
        self.document.update(self.valDict)

class JobAutoTunerView:

    """
    A Class Used to plot in a simple manner the effect from the
    JobAutoTuner modifications.
    """

    def __init__(self):
        self.startTime = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.histOutputIdle = "/home/grid/EstimatedWallTimeMinsIdle.jpg"
        self.histOutputRunning = "/home/grid/EstimatedWallTimeMinsRunning.jpg"
        self.histOutputPilots = "/home/grid/PilotRestLifeTime.jpg"
        self.histOutput = "/home/grid/EstimatedWallTimeMins.jpg"
        self.histOutputIdleTimeIdle = "/home/grid/IdleTimeDistributionIdle.jpg"
        self.histOutputIdleTimeRunning = "/home/grid/IdleTimeDistributionRunning.jpg"
        self.destUrl = "http://monit-metrics:10012"
        self.title = ""
        self.wallTimeStep = 60 # (mins)
        self.binWidth = self.wallTimeStep
        self.minEstimatedWalltimeMins = 0
        self.maxEstimatedWalltimeMins = 72*self.wallTimeStep #(48 hours)
        self.binsRange = range(self.minEstimatedWalltimeMins,
                               self.maxEstimatedWalltimeMins + self.binWidth,
                               self.binWidth)

        self.jsonDocList = []
        self.binDict = dict()

        for key in self.binsRange:
            self.binDict[str(key)] = 0

        self.jsonDocRunning = dict(
                            producer="crab",
                            type="JobTimeTunerRunning",
                            hostname=gethostname()
                            )
        self.jsonDocRunning.update(self.binDict)

        self.jsonDocIdle = dict(
                            producer="crab",
                            type="JobTimeTunerIdle",
                            hostname=gethostname()
                            )
        self.jsonDocIdle.update(self.binDict)

        self.jsonDocPilot = dict(
                            producer="crab",
                            type="PilotRestLifeTime",
                            hostname=gethostname()
                            )
        self.jsonDocPilot.update(self.binDict)

        matplotlib.pyplot.figure(num=None, figsize=(15, 12), dpi=80)


    def jobAutoTunerHist(self, arr, color, output, label ):
        # self.title="EstimatedWallTimeMins per (Idle | Running) jobs: %s" % self.startTime
        title="%s: %s" % (self.title ,self.startTime)
        self.label = label
        self.output = output
        self.color = color
        self.arr = arr
        self.n = None
        self.patches = None
        # self.binWidth = self.wallTimeStep
        self.bins = None
        # self.binsRange = range(self.minEstimatedWalltimeMins, self.maxEstimatedWalltimeMins  + self.binWidth, self.binWidth)
        # self.binsRange = range(min(self.arr), max(self.arr) + self.binWidth, self.binWidth)
        # matplotlib.pyplot.cla() # to clear the canvas from the previous plot


        # matplotlib.pyplot.plot()
        self.n, self.bins, self.patches = matplotlib.pyplot.hist(self.arr,
                                                                 facecolor=self.color, 
                                                                 alpha=0.75,
                                                                 label=self.label,
                                                                 bins=self.binsRange,
                                                                 align='left')

        matplotlib.pyplot.legend(loc='upper right')
        matplotlib.pyplot.title(title)
        matplotlib.pyplot.xlabel('Minutes')
        matplotlib.pyplot.xticks(self.bins, rotation = 'vertical', color='r')
        matplotlib.pyplot.minorticks_on()
        matplotlib.pyplot.ylabel('Number of jobs')
        # rescale the axes the max between the previous plot and the current one
        axes = matplotlib.pyplot.gca()
        ycurr = np.amax(self.n)
        ycurr = ycurr + int(3*(ycurr/100))
        yprev = int(max(axes.get_ylim()))
        ymax = max(yprev, ycurr)
        axes.set_ylim([0,ymax])
        matplotlib.pyplot.savefig(self.output, dpi=None, orientation='landscape', format='png')

    def jobAutoTunerJsonCreate(self, arr, jsonDoc):
        """
        A function used to create the Json docs for sending them to ElasticSearch
        """
        for time in arr:
            for key, value in jsonDoc.iteritems():
                if key.isdigit():
                    if time >= int(key) and time < (int(key) + self.binWidth):
                        newvalue = int(value)
                        newvalue += 1
                        jsonDoc[key] = int(newvalue)

        # renaming(prefixing) the keys in the dict.
        keyList = deque(jsonDoc)
        for key in keyList:
            if key.isdigit():
                jsonDoc["EWT_%s" % key] = jsonDoc[key]
                del jsonDoc[key]

        return jsonDoc


    def send(self, jsonDoc):
        return requests.post(self.destUrl,
                             data=json.dumps(jsonDoc),
                             headers={"Content-Type": "application/json; charset=UTF-8"})

    def jsonDocSend(self, jsonDoc, should_fail=False):
        response = self.send(jsonDoc)
        assert ( (response.status_code in [200]) != should_fail), 'With document: {0}. Status code: {1}. Message: {2}'.format(jsonDoc, response.status_code, response.text)


    def run(self):
        """
        The function used to run the class.
        """
        logging.info("JoubAutoTunerView.py has been started at: %s" % self.startTime)

        # what's the difference between: cmssrv221.fnal.gov:9618" | cmssrv221.fnal.gov:9620"
        collector = htcondor.Collector( "cmssrv221.fnal.gov:9618")
        schedd = htcondor.Schedd()

        # Estimating how many jobs has been TimngTuned
        constr = """
        (HasBeenTimingTuned isnt Undefined) &&
        (JobUniverse =?= 5 &&
        (JobStatus == 1 || JobStatus == 2 )) &&
        (CRAB_UserHN != "sciaba")
        """
        projection = [ "ClusterId",
                       "MaxWallTimeMins",
                       "EstimatedWallTimeMins",
                       "JobStatus" ]

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn( e,category=ScheddAlarm)

        jobAd = None
        # Creating the arrays to be plotted in histograms.
        estimatedWallTimeArrayIdle = array.array('i')
        estimatedWallTimeArrayRunning = array.array('i')
        for jobAd in results:
            keyAlarm = None
            logging.debug("Adding: %s" % jobAd['ClusterId'])
            if not 'EstimatedWallTimeMins' in jobAd.keys():
                keyAlarm = KeyAlarm(key='EstimatedWallTimeMins', clusterId=jobAd['ClusterId'])
                warnings.warn(keyAlarm, stacklevel=5)
            else:
                if   jobAd['JobStatus'] == 1:
                    estimatedWallTimeArrayIdle.append(jobAd['EstimatedWallTimeMins'])
                elif jobAd['JobStatus'] == 2:
                    estimatedWallTimeArrayRunning.append(jobAd['EstimatedWallTimeMins'])

        logging.debug(estimatedWallTimeArrayIdle)
        logging.debug(estimatedWallTimeArrayRunning)

        # Estimating how many jobs has NOT been TimngTuned
        constr = """
        (HasBeenTimingTuned is Undefined) &&
        (JobUniverse =?= 5 &&
        (JobStatus == 1 || JobStatus == 2 )) &&
        (CRAB_UserHN != "sciaba")
        """
        projection = [ "ClusterId",
                       "MaxWallTimeMins",
                       "EstimatedWallTimeMins",
                       "JobStatus" ]

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn( e,category=ScheddAlarm)

        jobAd = None
        # Creating the arrays to be plotted in histograms.
        estimatedWallTimeArrayIdleNotTuned = array.array('i')
        estimatedWallTimeArrayRunningNotTuned = array.array('i')
        for jobAd in results:
            keyAlarm = None
            logging.debug("Adding: %s" % jobAd['ClusterId'])
            if not 'EstimatedWallTimeMins' in jobAd.keys():
                keyAlarm = KeyAlarm(key='EstimatedWallTimeMins', clusterId=jobAd['ClusterId'])
                warnings.warn(keyAlarm, stacklevel=5)
            else:
                if   jobAd['JobStatus'] == 1:
                    estimatedWallTimeArrayIdleNotTuned.append(jobAd['EstimatedWallTimeMins'])
                elif jobAd['JobStatus'] == 2:
                    estimatedWallTimeArrayRunningNotTuned.append(jobAd['EstimatedWallTimeMins'])

        logging.debug(estimatedWallTimeArrayIdleNotTuned)
        logging.debug(estimatedWallTimeArrayRunningNotTuned)

        self.title="EstimatedWallTimeMins per (Idle | Running) jobs"
        # done - to make them plotted on a single histogram
        if not len(estimatedWallTimeArrayIdle) == 0:
            self.jobAutoTunerHist(arr=estimatedWallTimeArrayIdle,
                                  color='green',
                                  output=self.histOutput,
                                  label="Idle jobs Tuned")

        if not len(estimatedWallTimeArrayIdleNotTuned) == 0:
            self.jobAutoTunerHist(arr=estimatedWallTimeArrayIdleNotTuned,
                                  color='blue',
                                  output=self.histOutput,
                                  label="Idle jobs Not Tuned")

        if not len(estimatedWallTimeArrayRunning) == 0:
            self.jobAutoTunerHist(arr=estimatedWallTimeArrayRunning,
                                  color='red',
                                  output=self.histOutput,
                                  label="Running jobs Tuned")


        if not len(estimatedWallTimeArrayRunningNotTuned) == 0:
            self.jobAutoTunerHist(arr=estimatedWallTimeArrayRunningNotTuned,
                                  color='magenta',
                                  output=self.histOutput,
                                  label="Running jobs Not Tuned")


        # fill all the values in the jsonDoc
        self.jsonDocList.append(self.jobAutoTunerJsonCreate(arr=estimatedWallTimeArrayIdle, 
                                                            jsonDoc=self.jsonDocIdle))

        self.jsonDocList.append(self.jobAutoTunerJsonCreate(arr=estimatedWallTimeArrayRunning, 
                                                            jsonDoc=self.jsonDocRunning))

        ########################################################################
        ###### The following has been outdated - it was relying in a really big
        ###### query to the collector.In the new method we rely on the fact that
        ###### 'GLIDEIN_ToDie', 'GLIDEIN_ToRetire' classads are already known to
        ###### the running job (propagated back to it)
        ######
        # # Estimating how many short living pilots have been filled with tuned jobs
        # try:
        #     results = schedd.query('(HasBeenTimingTuned isnt Undefined) && (Jobuniverse == 5) && (Jobstatus == 2)', ["RemoteHost"])
        # except Exception, e:
        #     warnings.warn(e,category=ScheddAlarm)

        # # creating the constraint with all the pilot names
        # constr = '(Name == "INIT_STRING_SHOULD_NEVER_MATCH")'
        # for jobAd in results:
        #     constr = '(Name == "%s") || %s' % (jobAd["RemoteHost"], constr)

        # try:
        #     results = collector.query( htcondor.AdTypes.Any, constr , ['GLIDEIN_ToDie', 'GLIDEIN_ToRetire', 'EnteredCurrentState', 'EnteredCurrentActivity'] ) 
        # except Exception, e:
        #     warnings.warn( e ,category=ScheddAlarm)

        # # we need only the difference between the moment at which the modified job has landed into the pilot(EnteredCurrentState) and walltime of the pilot GLIDEIN_ToDie
        # pilotRestLifeTime = array.array('i')
        # for clasAd in results:
        #     restLifeTime = int((clasAd['GLIDEIN_ToDie'] - clasAd['EnteredCurrentState'])/60)
        #     pilotRestLifeTime.append(restLifeTime)
        ########################################################################

        # Estimating how many short living pilots have been filled with tuned jobs
        # here we exclude (DESIRED_Sites=!="T3_US_FNALLPC") because these are jobs 
        # routed by the LPCUsers Route and are running without pilots
        # adding LastMatchTime - to be sure that the jobs have been actually matched 
        constr = """
        (HasBeenTimingTuned isnt Undefined) &&
        (Jobuniverse == 5) &&
        (Jobstatus == 2) &&
        (DESIRED_Sites =!= "T3_US_FNALLPC") &&
        (LastMatchTime isnt Undefined)
        """
        projection = ['ClusterId',
                      'RemoteHost',
                      'MATCH_GLIDEIN_ToDie',
                      'MATCH_GLIDEIN_ToRetire',
                      'EnteredCurrentStatus']

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn(e,category=ScheddAlarm)

        # we need only the difference between the moment at which the modified
        # job has landed into the pilot(EnteredCurrentStatus)
        # and walltime of the pilot MATCH_GLIDEIN_ToDie
        pilotRestLifeTime = array.array('i')
        for clasAd in results:
            logging.debug("Adding: %s" % clasAd['ClusterId'])
            if not 'MATCH_GLIDEIN_ToDie' in clasAd.keys():
                keyAlarm = KeyAlarm(key='MATCH_GLIDEIN_ToDie', clusterId=clasAd['ClusterId'])
                warnings.warn(keyAlarm, stacklevel=5)
            else:
                restLifeTime = int((clasAd['MATCH_GLIDEIN_ToDie'] - clasAd['EnteredCurrentStatus'])/60)
                pilotRestLifeTime.append(restLifeTime)

        # Estimating the pilots lifetime distribution for not tuned jobs
        constr = """
        (HasBeenTimingTuned is Undefined) &&
        (Jobuniverse == 5) &&
        (Jobstatus == 2) &&
        (DESIRED_Sites =!= "T3_US_FNALLPC") &&
        (LastMatchTime isnt Undefined)
        """
        projection = ['ClusterId',
                      'RemoteHost',
                      'MATCH_GLIDEIN_ToDie',
                      'MATCH_GLIDEIN_ToRetire',
                      'EnteredCurrentStatus']

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn(e,category=ScheddAlarm)

        pilotRestLifeTimeNotTuned = array.array('i')
        for clasAd in results:
            logging.debug("Adding: %s" % clasAd['ClusterId'])
            if not 'MATCH_GLIDEIN_ToDie' in clasAd.keys():
                keyAlarm = KeyAlarm(key='MATCH_GLIDEIN_ToDie', clusterId=clasAd['ClusterId'])
                warnings.warn(keyAlarm, stacklevel=5)
            else:
                restLifeTime = int((clasAd['MATCH_GLIDEIN_ToDie'] - clasAd['EnteredCurrentStatus'])/60)
                pilotRestLifeTimeNotTuned.append(restLifeTime)



        # The histograms:
        self.title="Pilot Life Time Running jobs"
        matplotlib.pyplot.cla()         # clear  the canvas:
        if not len(pilotRestLifeTime) == 0:
            self.jobAutoTunerHist(arr=pilotRestLifeTime,
                                  color='green',
                                  output=self.histOutputPilots,
                                  label="PilotRestLifetime Tuned jobs")

        if not len(pilotRestLifeTimeNotTuned) == 0:
            self.jobAutoTunerHist(arr=pilotRestLifeTimeNotTuned,
                                  color='blue',
                                  output=self.histOutputPilots,
                                  label="PilotRestLifetime Not Tuned jobs")


        # Estimating the JobIdle time distribution per schedd for currently Idle jobs
        constr = """
        (HasBeenTimingTuned isnt Undefined) &&
        (Jobuniverse == 5) &&
        (Jobstatus == 1)
        """
        projection = ['ClusterId',
                      'ServerTime',
                      'QDate']

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn(e,category=ScheddAlarm)

        idleTimeTunedIdle = array.array('i')
        for clasAd in results:
            idleTime = int((clasAd['ServerTime'] - clasAd['QDate'])/60)
            idleTimeTunedIdle.append(idleTime)

        # Estimating the JobIdle time distribution per schedd for currently Idle jobs
        constr = """
        (HasBeenTimingTuned is Undefined) &&
        (Jobuniverse == 5) &&
        (Jobstatus == 1)
        """
        projection = ['ClusterId',
                      'ServerTime',
                      'QDate']

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn(e,category=ScheddAlarm)

        idleTimeNotTunedIdle = array.array('i')
        for clasAd in results:
            idleTime = int((clasAd['ServerTime'] - clasAd['QDate'])/60)
            idleTimeNotTunedIdle.append(idleTime)

        self.title="Idle Jobs Idle Time Disribution"

        matplotlib.pyplot.cla()
        if not len(idleTimeTunedIdle) == 0:
            self.jobAutoTunerHist(arr=idleTimeTunedIdle,
                                  color='green',
                                  output=self.histOutputIdleTimeIdle,
                                  label="Idle time Idle jobs Tuned")

        if not len(idleTimeNotTunedIdle) == 0:
            self.jobAutoTunerHist(arr=idleTimeNotTunedIdle,
                                  color='blue',
                                  output=self.histOutputIdleTimeIdle,
                                  label="Idle time Idle jobs Not Tuned")

        # fill all the values in the jsonDoc
        self.jsonDocList.append(self.jobAutoTunerJsonCreate(arr=pilotRestLifeTime,
                                                            jsonDoc=self.jsonDocPilot))







        # Estimating the JobIdle time distribution per schedd for currently Running jobs
        constr = """
        (HasBeenTimingTuned isnt Undefined) &&
        (Jobuniverse == 5) &&
        (Jobstatus == 2) &&
        (LastMatchTime isnt Undefined)
        """
        projection = ['ClusterId',
                      'ServerTime',
                      'JobStartDate',
                      'QDate']

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn(e,category=ScheddAlarm)

        idleTimeTunedRunning = array.array('i')
        for clasAd in results:
            idleTime = int((clasAd['JobStartDate'] - clasAd['QDate'])/60)
            idleTimeTunedRunning.append(idleTime)

        # Estimating the JobIdle time distribution per schedd for currently Running jobs
        constr = """
        (HasBeenTimingTuned is Undefined) &&
        (Jobuniverse == 5) &&
        (Jobstatus == 2) &&
        (LastMatchTime isnt Undefined)
        """
        projection = ['ClusterId',
                      'ServerTime',
                      'JobStartDate',
                      'QDate']

        try:
            results = schedd.query(constr, projection)
        except Exception, e:
            warnings.warn(e,category=ScheddAlarm)

        idleTimeNotTunedRunning = array.array('i')
        for clasAd in results:
            idleTime = int((clasAd['JobStartDate'] - clasAd['QDate'])/60)
            idleTimeNotTunedRunning.append(idleTime)

        self.title="Running Jobs Idle Time Disribution"

        matplotlib.pyplot.cla()
        if not len(idleTimeTunedRunning) == 0:
            self.jobAutoTunerHist(arr=idleTimeTunedRunning,
                                  color='red',
                                  output=self.histOutputIdleTimeRunning,
                                  label="Idle time Running jobs Tuned")

        if not len(idleTimeNotTunedRunning) == 0:
            self.jobAutoTunerHist(arr=idleTimeNotTunedRunning,
                                  color='magenta',
                                  output=self.histOutputIdleTimeRunning,
                                  label="Idle time Running jobs Not Tuned")

        # fill all the values in the jsonDoc
        self.jsonDocList.append(self.jobAutoTunerJsonCreate(arr=pilotRestLifeTime,
                                                            jsonDoc=self.jsonDocPilot))




        # pprint.pprint(self.jsonDocList)

        # ## sending the jsonDoc
        # try:
        #     self.jsonDocSend(self.jsonDocList)
        # except Exception as e:
        #     logging.warning(e)





def main():
    jobAutoTunerView = JobAutoTunerView()
    jobAutoTunerView.run()

if __name__ == "__main__":
    warnings.resetwarnings()
    warnings.simplefilter("always", category=KeyAlarm, lineno=0, append=False)
    warnings.simplefilter("error", category=ScheddAlarm, lineno=0, append=False)

    logFormatter = logging.Formatter('%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s')
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    # Setting different loglevels for file logging and consoleoutput
    fileHandler = logging.FileHandler(LOG_FILE)
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.INFO)
    rootLogger.addHandler(fileHandler)

    # Setting the output of the StreamHandler to stdout in order for the alarms to be catched from the cron.wrapper.sh
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.WARNING)
    rootLogger.addHandler(consoleHandler)

    # Temporary redirect stderr
    sys.stderr = open('/dev/null', 'w')

    logging.basicConfig(filename=LOG_FILE,level=logging.INFO,format='%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s')
    main()
