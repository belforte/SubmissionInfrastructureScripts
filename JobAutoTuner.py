#!/usr/bin/python

"""
This Script is intended to load separately the different scripts related
to job modifications done through the  condor JobRouter in the CRAB3 SChedd.
it is done in the following order:
1. JobTimeTuner.py
2. CMSLPCRoute.py
"""

import JobTimeTuner
import CMSLPCRoute
import logging
import warnings
import htcondor

from JobAutoTunerAlarms import *
from distutils.util import strtobool

from Overflow import Overflow

LOG_FILE = '/var/log/crab/JobAutoTuner.log'
ENABLE_JOBTIMETUNER = False
ENABLE_CMSLPCROUTE = False

def readCondorFlag(condorFlag):
    returnFlag = False
    if condorFlag not in htcondor.param.keys():
        alarm=ConfigAlarm(configKey=condorFlag, errMessage="Config key not set in condor.")
        warnings.warn(alarm)
    else:
        try:
            returnFlag = strtobool(htcondor.param.get(condorFlag))
        except Exception as e:
            alarm=ConfigAlarm(configKey=condorFlag,errMessage=e)
            warnings.warn(alarm)
    return returnFlag

def main():
    
    global ENABLE_CMSLPCROUTE
    global ENABLE_JOBTIMETUNER

    logging.info("=========================================================================")
    
    # 1. JobTimeTuner.py
    ENABLE_JOBTIMETUNER = readCondorFlag("JAT_ENABLE_JOBTIMETUNER")
    if ENABLE_JOBTIMETUNER:
        logging.info("-------------------- Routes added by JobTimeTuner.py --------------------") 
        try:
            jobTimeTuner = JobTimeTuner.JobTimeTuner()
            jobTimeTuner.run()
        except Exception as e:
            subScriptAlarm = SubScriptAlarm("ScriptName: %s\n" % "JobTimeTuner", e)
            warnings.warn(subScriptAlarm, stacklevel=5)
    else:
        logging.info("-------------------- JobTimeTuner.py was not enabled --------------------")

    # 2. CMSLPCRoute
    ENABLE_CMSLPCROUTE = readCondorFlag("JAT_ENABLE_CMSLPCROUTE")
    if ENABLE_CMSLPCROUTE:
        logging.info("-------------------- Routes added by CMSLPCRoute.py  --------------------") 
        try:
            lpc_users = CMSLPCRoute.fetchLPCUsers()
            CMSLPCRoute.generateLPCRoute(lpc_users)
            CMSLPCRoute.holdNonLPCUsers(lpc_users)
        except Exception as e:
            subScriptAlarm = SubScriptAlarm("ScriptName: %s" % "CMSLPCRoute", e)
            warnings.warn(subScriptAlarm, stacklevel=5)
    else:
        logging.info("-------------------- CMSLPCRoute.py was not enabled  --------------------")

    # 3. Overflow
    ENABLE_OVERFLOW = readCondorFlag("JAT_ENABLE_OVERFLOW")
    if ENABLE_OVERFLOW:
        logging.info("-------------------- Routes added by Overflow.py  --------------------") 
        try:
            overflow = Overflow.Overflow()
            overflow.run()
        except Exception as e:
            subScriptAlarm = SubScriptAlarm("ScriptName: %s" % "Overflow", e)
            warnings.warn(subScriptAlarm, stacklevel=5)
    else:
        logging.info("-------------------- Overflow.py was not enabled  --------------------")


    
    logging.info("=========================================================================")

if __name__ == "__main__":
    warnings.resetwarnings()
    warnings.simplefilter("always", category=GwmsmonAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=ConfigAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=ScheddAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=SubScriptAlarm, lineno=0, append=False)

    fromatString = '%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s'
    logFormatter = logging.Formatter(fromatString)
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    # Setting different loglevels for file logging and console output
    fileHandler = logging.FileHandler(LOG_FILE)
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.INFO)
    rootLogger.addHandler(fileHandler)

    # Setting the output of the StreamHandler to stdout
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.CRITICAL)
    rootLogger.addHandler(consoleHandler)

    # Temporary redirect stderr
    sys.stderr = open('/dev/null', 'w')

    logging.basicConfig(filename=LOG_FILE,level=logging.INFO,format=fromatString)

    main()
