import pprint
import warnings
from JobCleanupAlarms import *

# CrabDag is a 'container' holding all the dags 
# It will be invoked only for jobs with JobUNiverse == 7 
# by passing an object of type classad.ClassAd
# and all the subsiquent jobs (JobUniverse == 5) 
# will be added inside this object as a list of classad.ClassAd obects in self.jobs by a method addJob
# here htcDag stands for HTCondorDag (just to avoid confusion - This one is comming from HTCondor, The corresponding CrabDag is related only to the job structure in the CRAB system)
# 3 flags are introduced:
# isEmpty - when the task has no children 
# isComplete - when we are sure that there are no more jobs from this dag in the current schedd (should need a check for the number of children in the condor classadd (DAG_NodesQueued) and the actual len of the list self.jobs. We can find a few tasks in which these two numbers are not equal as we expect - this is a definite sign that part of the dag (or the whole dag) has been deleted or corrupted at some moment in time.

import pdb
class CrabDag:
    def __init__(self, htcDag):
        self.index = 0
        self.jobs = []
        self.task = htcDag
        self.clusterId = htcDag["ClusterId"]
        if not 'CRAB_ReqName' in self.task.keys():
            self.name = None
            raise KeyAlarm(key='CRAB_ReqName', clusterId=self.clusterId)
        else:
            self.name = htcDag["CRAB_ReqName"]
        self.isEmpty = False
        self.isComplete = False
        self.isReadyToClear = False
        self.isCleared = False
        self.dagLifeTime = 0
        self.dagExtendLifeTime = 0

    def addJob(self, htcDag):
        self.jobs.append(htcDag)

    def __repr__(self):
        return ("CrabDag: %s\n" % pprint.pformat(self.task))
