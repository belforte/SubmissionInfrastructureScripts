#!/bin/sh

FREE_MEMORY=$(grep ^MemFree /proc/meminfo | awk '{print $2}')
BUFFER_MEMORY=$(grep ^Buffers /proc/meminfo | awk '{print $2}')
CACHED_MEMORY=$(grep ^Cached /proc/meminfo | awk '{print $2}')
TOTAL_FREE_MEMORY=$((FREE_MEMORY + BUFFER_MEMORY + CACHED_MEMORY))
echo "TotalFreeMemoryMB = $((TOTAL_FREE_MEMORY / 1024))"

