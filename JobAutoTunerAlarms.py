#!/usr/bin/python

"""
Alarm and warning definitions for the JobAutoTuner.
"""

import sys
import os
import pwd
import json
import pprint
import logging
import warnings
import pdb

from datetime import datetime


class Alarm(Warning):
    """Base class for all the alarms raised by this script(module)."""
    pass

class GwmsmonAlarm(Alarm):
    """
    An Alarm raised if gwmsmon.cern.ch returned an empty list of jobs/tasks.
    (this alarm is an 'always' executed warning which can easyly be converted to a real exception by
    setting this to 'error' in warnings.filterwarnings())

    Attributes:
    message -- explanation of the error
    """
    # todo - here to send emails to the operator or lemon alarms
    def __init__(self, message):
        self.message = message
	self.time = str(datetime.now())
        alarmMessage = ("WARNING: GwmsmonAlarm: at: %s / additional info: %s" % (self.time, self.message))
        logging.warning(alarmMessage)

class ScheddAlarm(Alarm):
    """
    An Alarm raised if we couldn't make `condor_q` for any reason.
    (this alarm is an 'error' warning which is a real exception this could be  changed by
    setting this to 'always/default/once/ignore/module' in warnings.filterwarnings())

    Attributes:
    message -- explanation of the error
    """
    # todo - here to send emails to the operator or lemon alarms
    def __init__(self, message):
        self.message = message
        self.time = str(datetime.now())
        alarmMessage = ("ERR: Could not query the schedd at: %s / additional info: %s" % (self.time,self.message))
        logging.error(alarmMessage)

class ConfigAlarm(Alarm):
    """
    An Alarm raised if we couldn't find the config parameters in htcondor for any reason.

    Attributes:
    configKey -- config paramater that is searched in condor_config_val
    message -- additional info for the alarm
    """
    # todo - here to send emails to the operator or lemon alarms
    def __init__(self, configKey, errMessage ):
        self.configKey = configKey
        self.errMessage = errMessage
        alarmMessage = ("ERR: Could not find/read the config parameter: %s. Not enabling the service! Additional info: %s" % (self.configKey, self.errMessage))
        logging.error(alarmMessage)


class KeyAlarm(Alarm):
    """
    An Alarm raised for errors concerning missing keys in the task classads found in the current schedd.
    (JobUniverse == 7)

    Attributes:
    key       -- the missing key
    clusterId -- the schedd clusterId of the job raising the alarm
    """
    def __init__(self, key, clusterId):
        self.key = key
        self.clusterId = clusterId
        self.alarmMessage = ("KeyAlarm: Missing key ['%s'] for the task with clusterId: %s" % (self.key, self.clusterId))
        logging.warning(self.alarmMessage)
        # print("KeyAlarm: %s" % self.alarmMessage)

class SubScriptAlarm(Alarm):
    """
    An Alarm raised if one of the subscripts exit with a nonZero status.
    """
    def __init__(self, message, exc=None):
        self.message = message
        self.exc = exc
	self.time = str(datetime.now())
        alarmMessage = ("ERR: SubScriptAlarm: at: %s / additional info: \n%s\nException: %s" % (self.time, self.message,self.exc))
        logging.exception(alarmMessage)


class SiteLimitsAlarm(Alarm):
    """
    A general SiteLimits Alarm:
    It is intended to notify the operator that the SiteLimits from the Overflow
    script are not counted correctly. Actions are expected here because otherwise
    the sites can be easily flooded.

    Attributes:
    message -- The message to be sent to the Operator.
    """
    def __init__(self, message):
        pass
